use super::connection::{Connection, ConnectionReceiver};

static BOT_NAME: &'static str = "Om_Nom_Nom_Cookies";
static BOT_OAUTH: &'static str = "oauth:0yuuft3g9wszkvnmd523ejrxfcdeof";

pub struct BotConfig {
    pass: String,
    nick: String,
    channels: Vec<String>,
}

pub struct TwitchBot;

impl TwitchBot {
    pub fn run(self, conn: &mut Connection) {
        conn.send(format!("USER {user} 8 * :{user}\r\n", user = BOT_NAME).as_bytes());
        conn.send(format!("PASS {}\r\n", BOT_OAUTH).as_bytes());
        conn.send(format!("NICK {}\r\n", BOT_NAME).as_bytes());
        conn.send("JOIN #tomasmorrow\r\n".as_bytes());
        conn.send("\r\n".as_bytes());

        conn.connect(&self);
    }

    fn parse(&self, data: &[u8]) -> Option<&[u8]> {
        let message = String::from_utf8_lossy(&data);
        println!("{}", message);

        if message.contains("PING") {
            println!("PONG.");

            return Some("PONG :tmi.twitch.tv\r\n".as_bytes());
        }

        None
    }

//    fn find_handler(&self, message: &Message) {
//
//    }
}

impl ConnectionReceiver for TwitchBot {
    fn receive(&self, data: &[u8]) -> Option<&[u8]> {
        return self.parse(data);
    }
}

//struct Message {
//
//};

fn parse(message: String) -> String {
    message.clone()
}

#[cfg(test)]
mod tests {
    use super::parse;

    #[test]
    fn test_parser() {
        assert_eq!(parse(String::from("hello")), String::from("hello"))
    }
}


//
//struct IRCClient {}
//
//struct Channel {
//    topic: String,  // 200 characters
//}
//
//const SPACE : u8 = 0x20;
//const PREFIX : u8 = 0x3b;
//const CR_LF : [u8; 2] = [0xd, 0xa];
//
//struct Command {
//    name: String,
//}
//
//struct CommandParameter {
//    middle: String,
//}
//
//struct Prefix {
//    server_name: String,
//    nick_name: String,  // 9 characters
//    user: String,
//    host: String,
//}
//
//struct Message {
//    prefix: Option<Prefix>,
//    command: Command,
//    parameters: Vec<CommandParameter>,
//}
//
//fn password_message(password : String) -> Message {
//    let command_parameters = vec!();
//
//    command_parameters.push(CommandParameter{middle: password});
//
//    Message {
//        command: Command {
//            name: "PASS"
//        },
//        parameters: command_parameters,
//    }
//}