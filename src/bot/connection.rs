use std::io::prelude::*;
use std::net::TcpStream;

pub trait ConnectionReceiver {
    fn receive(&self, data: &[u8]) -> Option<&[u8]>;
}

pub struct ConnectionConfig {
    pub server: String,
    pub port: String,
}

pub struct Connection {
    tcp_conn: TcpStream,
}

impl Connection {
    pub fn new(config: &ConnectionConfig) -> Connection {
        let full_server_name: &str = &format!("{server}:{port}",
        server = config.server,
        port = config.port);

        Connection {
            tcp_conn: TcpStream::connect(full_server_name).unwrap(),
        }
    }

    pub fn connect<T: ConnectionReceiver>(&mut self, receiver: &T) {
        loop {
            let mut buf = [0; 1024];
            let read_count = self.tcp_conn.read(&mut buf);

            match read_count {
                Ok(len) => {
                    if len > 0 {
                        match receiver.receive(&buf) {
                            Some(answer) => {
                                self.send(&answer);
                            },
                            None => {}
                        }
                    } else {
                        println!("Empty data");
                    }
                },
                Err(e) => {
                    println!("Stream is ended: {}", e);
                    break;
                }
            }
        }

        println!("Disconnected");
    }

    pub fn send(&mut self, data: &[u8]) {
        self.tcp_conn.write(data);
    }
}