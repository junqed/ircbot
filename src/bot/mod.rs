use self::parser::TwitchBot;
use self::connection::{ConnectionConfig, Connection};

mod parser;
mod connection;

pub fn run() {
    let bot = TwitchBot;

    let conn_config = ConnectionConfig {
        server: String::from("irc.twitch.tv"),
        port: String::from("6667"),
    };

    let mut connection = Connection::new(&conn_config);
    bot.run(&mut connection);
}