/// Various prefixes extracted from RFC1459.
pub mod prefixes {
    /// Normal channel
    pub const CHANNEL: &'static str = "#";

    /// Distributed channel
    pub const DISTRIBUTED: &'static str = "&";


    /// Channel owner +q (non-standard)
    pub const OWNER: &'static str = "~";

    /// Channel admin +a (non-standard)
    pub const ADMIN: &'static str = "&";

    /// Channel operator +o
    pub const OPERATOR: &'static str = "@";

    /// Channel half operator +h (non-standard)
    pub const HALF_OPERATOR: &'static str = "%";

    /// User has voice +v
    pub const VOICE: &'static str = "+";
}

/// User modes as defined by RFC1459 section 4.2.3.2.
pub mod user_mods {
    /// User is invisible
    pub const INVISIBLE: &'static str = "i";

    /// User wants to receive server notices
    pub const SERVER_NOTICES: &'static str = "s";

    /// User wants to receive Wallops
    pub const WALL_OPS: &'static str = "w";

    /// Server operator
    pub const MODE_OPERATOR: &'static str = "o";
}

/// Channel modes as defined by RFC1459 section 4.2.3.1
pub mod channel_modes {
    /// Operator privileges
    pub const OPERATOR: &'static str = "o";

    /// Ability to speak on a moderated channel
    pub const VOICE: &'static str = "v";

    /// Private channel
    pub const PRIVATE: &'static str = "p";

    /// Secret channel
    pub const SECRET: &'static str = "s";

    /// Users can't join without invite
    pub const INVITE_ONLY: &'static str = "i";

    /// Topic can only be set by an operator
    pub const TOPIC: &'static str = "t";

    /// Only voiced users and operators can talk
    pub const MODERATED: &'static str = "m";

    /// User limit
    pub const LIMIT: &'static str = "l";

    /// Channel password
    pub const KEY: &'static str = "k";


    /// Owner privileges (non-standard)
    pub const OWNER: &'static str = "q";

    /// Admin privileges (non-standard)
    pub const ADMIN: &'static str = "a";

    /// Half-operator privileges (non-standard)
    pub const HALF_OPERATOR: &'static str = "h";
}

/// IRC commands extracted from RFC2812 section 3 and RFC2813 section 4.
pub mod irc_commands {
    pub const PASS: &'static str = "PASS";
    pub const NICK: &'static str = "NICK";
    pub const USER: &'static str = "USER";
    pub const OPER: &'static str = "OPER";
    pub const MODE: &'static str = "MODE";
    pub const SERVICE: &'static str = "SERVICE";
    pub const QUIT: &'static str = "QUIT";
    pub const SQUIT: &'static str = "SQUIT";
    pub const JOIN: &'static str = "JOIN";
    pub const PART: &'static str = "PART";
    pub const TOPIC: &'static str = "TOPIC";
    pub const NAMES: &'static str = "NAMES";
    pub const LIST: &'static str = "LIST";
    pub const INVITE: &'static str = "INVITE";
    pub const KICK: &'static str = "KICK";
    pub const PRIVMSG: &'static str = "PRIVMSG";
    pub const NOTICE: &'static str = "NOTICE";
    pub const MOTD: &'static str = "MOTD";
    pub const LUSERS: &'static str = "LUSERS";
    pub const VERSION: &'static str = "VERSION";
    pub const STATS: &'static str = "STATS";
    pub const LINKS: &'static str = "LINKS";
    pub const TIME: &'static str = "TIME";
    pub const CONNECT: &'static str = "CONNECT";
    pub const TRACE: &'static str = "TRACE";
    pub const ADMIN: &'static str = "ADMIN";
    pub const INFO: &'static str = "INFO";
    pub const SERVLIST: &'static str = "SERVLIST";
    pub const SQUERY: &'static str = "SQUERY";
    pub const WHO: &'static str = "WHO";
    pub const WHOIS: &'static str = "WHOIS";
    pub const WHOWAS: &'static str = "WHOWAS";
    pub const KILL: &'static str = "KILL";
    pub const PING: &'static str = "PING";
    pub const PONG: &'static str = "PONG";
    pub const ERROR: &'static str = "ERROR";
    pub const AWAY: &'static str = "AWAY";
    pub const REHASH: &'static str = "REHASH";
    pub const DIE: &'static str = "DIE";
    pub const RESTART: &'static str = "RESTART";
    pub const SUMMON: &'static str = "SUMMON";
    pub const USERS: &'static str = "USERS";
    pub const WALLOPS: &'static str = "WALLOPS";
    pub const USERHOST: &'static str = "USERHOST";
    pub const ISON: &'static str = "ISON";
    pub const SERVER: &'static str = "SERVER";
    pub const NJOIN: &'static str = "NJOIN";
}

pub mod numeric_replies {
    pub const RPL_WELCOME: &'static str = "001";
    pub const RPL_YOURHOST: &'static str = "002";
    pub const RPL_CREATED: &'static str = "003";
    pub const RPL_MYINFO: &'static str = "004";
    pub const RPL_BOUNCE: &'static str = "005";
    pub const RPL_ISUPPORT: &'static str = "005";
    pub const RPL_USERHOST: &'static str = "302";
    pub const RPL_ISON: &'static str = "303";
    pub const RPL_AWAY: &'static str = "301";
    pub const RPL_UNAWAY: &'static str = "305";
    pub const RPL_NOWAWAY: &'static str = "306";
    pub const RPL_WHOISUSER: &'static str = "311";
    pub const RPL_WHOISSERVER: &'static str = "312";
    pub const RPL_WHOISOPERATOR: &'static str = "313";
    pub const RPL_WHOISIDLE: &'static str = "317";
    pub const RPL_ENDOFWHOIS: &'static str = "318";
    pub const RPL_WHOISCHANNELS: &'static str = "319";
    pub const RPL_WHOWASUSER: &'static str = "314";
    pub const RPL_ENDOFWHOWAS: &'static str = "369";
    pub const RPL_LISTSTART: &'static str = "321";
    pub const RPL_LIST: &'static str = "322";
    pub const RPL_LISTEND: &'static str = "323";
    pub const RPL_UNIQOPIS: &'static str = "325";
    pub const RPL_CHANNELMODEIS: &'static str = "324";
    pub const RPL_NOTOPIC: &'static str = "331";
    pub const RPL_TOPIC: &'static str = "332";
    pub const RPL_INVITING: &'static str = "341";
    pub const RPL_SUMMONING: &'static str = "342";
    pub const RPL_INVITELIST: &'static str = "346";
    pub const RPL_ENDOFINVITELIST: &'static str = "347";
    pub const RPL_EXCEPTLIST: &'static str = "348";
    pub const RPL_ENDOFEXCEPTLIST: &'static str = "349";
    pub const RPL_VERSION: &'static str = "351";
    pub const RPL_WHOREPLY: &'static str = "352";
    pub const RPL_ENDOFWHO: &'static str = "315";
    pub const RPL_NAMREPLY: &'static str = "353";
    pub const RPL_ENDOFNAMES: &'static str = "366";
    pub const RPL_LINKS: &'static str = "364";
    pub const RPL_ENDOFLINKS: &'static str = "365";
    pub const RPL_BANLIST: &'static str = "367";
    pub const RPL_ENDOFBANLIST: &'static str = "368";
    pub const RPL_INFO: &'static str = "371";
    pub const RPL_ENDOFINFO: &'static str = "374";
    pub const RPL_MOTDSTART: &'static str = "375";
    pub const RPL_MOTD: &'static str = "372";
    pub const RPL_ENDOFMOTD: &'static str = "376";
    pub const RPL_YOUREOPER: &'static str = "381";
    pub const RPL_REHASHING: &'static str = "382";
    pub const RPL_YOURESERVICE: &'static str = "383";
    pub const RPL_TIME: &'static str = "391";
    pub const RPL_USERSSTART: &'static str = "392";
    pub const RPL_USERS: &'static str = "393";
    pub const RPL_ENDOFUSERS: &'static str = "394";
    pub const RPL_NOUSERS: &'static str = "395";
    pub const RPL_TRACELINK: &'static str = "200";
    pub const RPL_TRACECONNECTING: &'static str = "201";
    pub const RPL_TRACEHANDSHAKE: &'static str = "202";
    pub const RPL_TRACEUNKNOWN: &'static str = "203";
    pub const RPL_TRACEOPERATOR: &'static str = "204";
    pub const RPL_TRACEUSER: &'static str = "205";
    pub const RPL_TRACESERVER: &'static str = "206";
    pub const RPL_TRACESERVICE: &'static str = "207";
    pub const RPL_TRACENEWTYPE: &'static str = "208";
    pub const RPL_TRACECLASS: &'static str = "209";
    pub const RPL_TRACERECONNECT: &'static str = "210";
    pub const RPL_TRACELOG: &'static str = "261";
    pub const RPL_TRACEEND: &'static str = "262";
    pub const RPL_STATSLINKINFO: &'static str = "211";
    pub const RPL_STATSCOMMANDS: &'static str = "212";
    pub const RPL_ENDOFSTATS: &'static str = "219";
    pub const RPL_STATSUPTIME: &'static str = "242";
    pub const RPL_STATSOLINE: &'static str = "243";
    pub const RPL_UMODEIS: &'static str = "221";
    pub const RPL_SERVLIST: &'static str = "234";
    pub const RPL_SERVLISTEND: &'static str = "235";
    pub const RPL_LUSERCLIENT: &'static str = "251";
    pub const RPL_LUSEROP: &'static str = "252";
    pub const RPL_LUSERUNKNOWN: &'static str = "253";
    pub const RPL_LUSERCHANNELS: &'static str = "254";
    pub const RPL_LUSERME: &'static str = "255";
    pub const RPL_ADMINME: &'static str = "256";
    pub const RPL_ADMINLOC1: &'static str = "257";
    pub const RPL_ADMINLOC2: &'static str = "258";
    pub const RPL_ADMINEMAIL: &'static str = "259";
    pub const RPL_TRYAGAIN: &'static str = "263";
    pub const ERR_NOSUCHNICK: &'static str = "401";
    pub const ERR_NOSUCHSERVER: &'static str = "402";
    pub const ERR_NOSUCHCHANNEL: &'static str = "403";
    pub const ERR_CANNOTSENDTOCHAN: &'static str = "404";
    pub const ERR_TOOMANYCHANNELS: &'static str = "405";
    pub const ERR_WASNOSUCHNICK: &'static str = "406";
    pub const ERR_TOOMANYTARGETS: &'static str = "407";
    pub const ERR_NOSUCHSERVICE: &'static str = "408";
    pub const ERR_NOORIGIN: &'static str = "409";
    pub const ERR_NORECIPIENT: &'static str = "411";
    pub const ERR_NOTEXTTOSEND: &'static str = "412";
    pub const ERR_NOTOPLEVEL: &'static str = "413";
    pub const ERR_WILDTOPLEVEL: &'static str = "414";
    pub const ERR_BADMASK: &'static str = "415";
    pub const ERR_UNKNOWNCOMMAND: &'static str = "421";
    pub const ERR_NOMOTD: &'static str = "422";
    pub const ERR_NOADMININFO: &'static str = "423";
    pub const ERR_FILEERROR: &'static str = "424";
    pub const ERR_NONICKNAMEGIVEN: &'static str = "431";
    pub const ERR_ERRONEUSNICKNAME: &'static str = "432";
    pub const ERR_NICKNAMEINUSE: &'static str = "433";
    pub const ERR_NICKCOLLISION: &'static str = "436";
    pub const ERR_UNAVAILRESOURCE: &'static str = "437";
    pub const ERR_USERNOTINCHANNEL: &'static str = "441";
    pub const ERR_NOTONCHANNEL: &'static str = "442";
    pub const ERR_USERONCHANNEL: &'static str = "443";
    pub const ERR_NOLOGIN: &'static str = "444";
    pub const ERR_SUMMONDISABLED: &'static str = "445";
    pub const ERR_USERSDISABLED: &'static str = "446";
    pub const ERR_NOTREGISTERED: &'static str = "451";
    pub const ERR_NEEDMOREPARAMS: &'static str = "461";
    pub const ERR_ALREADYREGISTRED: &'static str = "462";
    pub const ERR_NOPERMFORHOST: &'static str = "463";
    pub const ERR_PASSWDMISMATCH: &'static str = "464";
    pub const ERR_YOUREBANNEDCREEP: &'static str = "465";
    pub const ERR_YOUWILLBEBANNED: &'static str = "466";
    pub const ERR_KEYSET: &'static str = "467";
    pub const ERR_CHANNELISFULL: &'static str = "471";
    pub const ERR_UNKNOWNMODE: &'static str = "472";
    pub const ERR_INVITEONLYCHAN: &'static str = "473";
    pub const ERR_BANNEDFROMCHAN: &'static str = "474";
    pub const ERR_BADCHANNELKEY: &'static str = "475";
    pub const ERR_BADCHANMASK: &'static str = "476";
    pub const ERR_NOCHANMODES: &'static str = "477";
    pub const ERR_BANLISTFULL: &'static str = "478";
    pub const ERR_NOPRIVILEGES: &'static str = "481";
    pub const ERR_CHANOPRIVSNEEDED: &'static str = "482";
    pub const ERR_CANTKILLSERVER: &'static str = "483";
    pub const ERR_RESTRICTED: &'static str = "484";
    pub const ERR_UNIQOPPRIVSNEEDED: &'static str = "485";
    pub const ERR_NOOPERHOST: &'static str = "491";
    pub const ERR_UMODEUNKNOWNFLAG: &'static str = "501";
    pub const ERR_USERSDONTMATCH: &'static str = "502";
}

// IRC commands extracted from the IRCv3 spec at http://www.ircv3.org/.
pub mod irc_commands_v3 {
    pub const CAP: &'static str = "CAP";

    /// Subcommand (param)
    pub const CAP_LS: &'static str = "LS";

    /// Subcommand (param)
    pub const CAP_LIST: &'static str = "LIST";

    /// Subcommand (param)
    pub const CAP_REQ: &'static str = "REQ";

    /// Subcommand (param)
    pub const CAP_ACK: &'static str = "ACK";

    /// Subcommand (param)
    pub const CAP_NAK: &'static str = "NAK";

    /// Subcommand (param)
    pub const CAP_CLEAR: &'static str = "CLEAR";

    /// Subcommand (param)
    pub const CAP_END: &'static str = "END";

    pub const AUTHENTICATE: &'static str = "AUTHENTICATE";
}

// Numeric IRC replies extracted from the IRCv3 spec.
pub mod numeric_replies_v3 {
    pub const RPL_LOGGEDIN: &'static str = "900";
    pub const RPL_LOGGEDOUT: &'static str = "901";
    pub const RPL_NICKLOCKED: &'static str = "902";
    pub const RPL_SASLSUCCESS: &'static str = "903";
    pub const ERR_SASLFAIL: &'static str = "904";
    pub const ERR_SASLTOOLONG: &'static str = "905";
    pub const ERR_SASLABORTED: &'static str = "906";
    pub const ERR_SASLALREADY: &'static str = "907";
    pub const RPL_SASLMECHS: &'static str = "908";
}
