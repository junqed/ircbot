use std::cmp;
use std::error::Error;
use std::fmt;
use std::mem::size_of_val;
use std::result;

/// Prefix or last argument
const PREFIX: u8 = 0x3A;

/// username
const PREFIX_USER: u8 = 0x21;

/// hostname
const PREFIX_HOST: u8 = 0x40;

/// separator
const SPACE: u8 = 0x20;

/// Maximum length is 512 - 2 for the line endings.
const MAX_LENGTH: u16 = 510;

#[derive(Clone, PartialEq, Debug)]
pub enum MessageError {
    PrefixParseError,
    MessageParseError,
}

impl fmt::Display for MessageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            MessageError::PrefixParseError => write!(f, "Prefix parse error"),
            MessageError::MessageParseError => write!(f, "Message parse error"),
        }
    }
}

impl Error for MessageError {
    fn description(&self) -> &str {
        match *self {
            MessageError::PrefixParseError => "Prefix parse error",
            MessageError::MessageParseError => "Message parse error",
        }
    }
}

pub type Result<T> = result::Result<T, MessageError>;

/// Prefix represents the prefix (sender) of an IRC message.
/// See RFC1459 section 2.3.1.
///
///    <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
///
pub struct Prefix {
    pub name: String,
    pub user: Option<String>,
    pub host: Option<String>,
}

impl Prefix {
    pub fn new<S>(name: S) -> Self where S: Into<String> {
        Prefix {
            name: name.into(),
            user: None,
            host: None,
        }
    }

    pub fn user<'a, S>(&'a mut self, user: S) -> &'a mut Self where S: Into<String> {
        self.user = Some(user.into());
        self
    }

    pub fn host<'a, S>(&'a mut self, host: S) -> &'a mut Self where S: Into<String> {
        self.host = Some(host.into());
        self
    }

    pub fn parse(raw: &str) -> Result<Prefix> {
        let (user, host) = try!(Prefix::parse_user_and_host(raw));
        let (nick, name) = try!(Prefix::parse_nick_and_name(user));

        Ok(Prefix {
            name: nick,
            user: name,
            host: host,
        })
    }

    pub fn len(&self) -> usize {
        let mut length = self.name.len();

        if let Some(ref val) = self.user {
            length += val.len() + size_of_val(&PREFIX_USER);
        }

        if let Some(ref val) = self.host {
            length += val.len() + size_of_val(&PREFIX_HOST);
        }

        length
    }

    pub fn is_hostmask(&self) -> bool {
        if let (&Some(_), &Some(_)) = (&self.user, &self.host) {
            true
        } else {
            false
        }
    }

    pub fn is_server(&self) -> bool {
        !self.is_hostmask()
    }

    fn parse_user_and_host(raw: &str) -> Result<(&str, Option<String>)> {
        let user_and_host: Vec<&str> = raw.split(PREFIX_HOST as char).collect();

        match user_and_host.len() {
            2 => {
                let (user, host) = (user_and_host[0], user_and_host[1]);

                if user.is_empty() || host.is_empty() {
                    return Err(MessageError::PrefixParseError);
                }

                Ok((user, Some(String::from(host))))
            },
            1 => {
                let user = user_and_host[0];

                if user.is_empty() {
                    return Err(MessageError::PrefixParseError);
                }

                Ok((user, None))
            },
            _ => Err(MessageError::PrefixParseError)
        }
    }

    fn parse_nick_and_name(full_name: &str) -> Result<(String, Option<String>)> {
        let nick_and_name: Vec<&str> = full_name.split(PREFIX_USER as char).collect();

        match nick_and_name.len() {
            2 => {
                let (nick, name) = (nick_and_name[0], nick_and_name[1]);

                if nick.is_empty() || name.is_empty() {
                    return Err(MessageError::PrefixParseError);
                }

                Ok((nick.to_string(), Some(name.to_string())))
            },
            1 => {
                let nick = nick_and_name[0];

                if nick.is_empty() {
                    return Err(MessageError::PrefixParseError);
                }

                Ok((nick.to_string(), None))
            },
            _ => Err(MessageError::PrefixParseError)
        }
    }
}

impl fmt::Display for Prefix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}", self.name));

        if let Some(ref user) = self.user {
            try!(write!(f, "{}{}", PREFIX_USER as char, user));
        }

        if let Some(ref host) = self.host {
            try!(write!(f, "{}{}", PREFIX_HOST as char, host))
        }

        Ok(())
    }
}

impl cmp::PartialEq for Prefix {
    fn eq(&self, other: &Prefix) -> bool {
        self.name == other.name && self.user == other.user && self.host == other.host
    }
}

/// Message represents an IRC protocol message.
/// See RFC1459 section 2.3.1.
///
///    <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
///    <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
///    <command>  ::= <letter> { <letter> } | <number> <number> <number>
///    <SPACE>    ::= ' ' { ' ' }
///    <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
///
///    <middle>   ::= <Any *non-empty* sequence of octets not including SPACE
///                   or NUL or CR or LF, the first of which may not be ':'>
///    <trailing> ::= <Any, possibly *empty*, sequence of octets not including
///                   NUL or CR or LF>
///
///    <crlf>     ::= CR LF
pub struct Message {
    pub prefix: Option<Prefix>,
    pub command: String,
    pub params: Vec<String>,
    pub trailing: Option<String>,
}

impl Message {
    pub fn new<S>(command: S) -> Self where S: Into<String> {
        Message {
            prefix: None,
            command: command.into(),
            params: vec![],
            trailing: None,
        }
    }

    pub fn prefix<'a>(&'a mut self, prefix: Prefix) -> &'a mut Self {
        self.prefix = Some(prefix);
        self
    }

    pub fn param<'a, S>(&'a mut self, param: S) -> &'a mut Self where S: Into<String> {
        self.params.push(param.into());
        self
    }

    pub fn params<'a, T>(&'a mut self, params: T) -> &'a mut Self
         where T: IntoIterator<Item=String> {
        self.params.extend(params);
        self
    }

    pub fn trailing<'a, S>(&'a mut self, trailing: S) -> &'a mut Self where S: Into<String> {
        self.trailing = Some(trailing.into());
        self
    }

    pub fn parse(raw: &str) -> Result<Message> {
        let mut trimmed_raw = raw.trim_matches(|c| c == '\r' || c == '\n');

        if trimmed_raw.len() < 2 {
            return Err(MessageError::MessageParseError);
        }

        let mut prefix = None;
        let mut command = "".to_owned();
        let mut trailing = None;

        // find a prefix at first
        if trimmed_raw.starts_with(PREFIX as char) {
            if let Some(prefix_end) = trimmed_raw.find(SPACE as char) {
                let (prefix_raw, trimmed_raw_) = trimmed_raw.split_at(prefix_end);

                prefix = Some(try!(Prefix::parse(
                    prefix_raw.trim_matches(|c| c == PREFIX as char || c == SPACE as char))
                              ));

                trimmed_raw = trimmed_raw_;
            } else {
                return Err(MessageError::MessageParseError);
            }
        }

        if trimmed_raw.len() <= 0 {
            return Err(MessageError::MessageParseError);
        }

        // then look for the trailing parameter
        if let Some(trailing_start) = trimmed_raw.find(&format!("{}{}", SPACE as char, PREFIX as char)) {
            let (params_raw, trailing_raw) = trimmed_raw.split_at(
                trailing_start + size_of_val(&PREFIX) + size_of_val(&SPACE));

            trailing = Some(String::from(trailing_raw));
            trimmed_raw = params_raw.trim_right_matches(PREFIX as char);
        }

        if trimmed_raw.len() <= 0 {
            return Err(MessageError::MessageParseError);
        }

        // then split the rest params
        let mut params: Vec<String> = trimmed_raw
        .split(SPACE as char)
        .map(|s| String::from(s.trim_matches(SPACE as char)))
        .filter(|s| s.len() > 0).collect();

        if params.len() > 0 {
            // the first param must be a command
            command = params.remove(0);
        } else {
            return Err(MessageError::MessageParseError);
        }

        Ok(Message {
            prefix: prefix,
            command: command,
            params: params,
            trailing: trailing,
        })
    }

    pub fn len(&self) -> usize {
        let mut length = self.command.len();

        if let Some(ref val) = self.prefix {
            length += val.len() + size_of_val(&PREFIX) + size_of_val(&SPACE);
        }

        length += self.params.iter().fold(0, |sum, val| sum + val.len() + size_of_val(&SPACE));

        if let Some(ref val) = self.trailing {
            length += val.len() + size_of_val(&PREFIX) + size_of_val(&SPACE);
        }

        length
    }
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // TODO: write to the formatter after checking the max length of the message

        if let Some(ref val) = self.prefix {
            try!(write!(f, "{}{}{}", PREFIX as char, val, SPACE as char));
        }

        try!(write!(f, "{}", self.command));

        if self.params.len() > 0 {
            try!(write!(f,
                        "{}{}",
                        SPACE as char,
                        self.params.join(&(SPACE as char).to_string())));
        }

        if let Some(ref val) = self.trailing {
            try!(write!(f, "{}{}{}", SPACE as char, PREFIX as char, val));
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{Prefix, Message};

    #[test]
    fn test_prefix_only_name() {
        let raw = "name";
        let (standard_prefix, test_prefix) = (Prefix::new("name"), Prefix::parse(raw));

        assert!(test_prefix.is_ok());

        let test_prefix = test_prefix.unwrap();
        assert!(standard_prefix == test_prefix);
        assert!(test_prefix.to_string() == raw.to_string());
        assert!(test_prefix.len() == raw.len());
        assert!(!test_prefix.is_hostmask());
        assert!(test_prefix.is_server());
    }

    #[test]
    fn test_prefix_name_and_user() {
        let raw = "name!user";
        let (standard_prefix, test_prefix) = (Prefix {
            name: String::from("name"),
            user: Some(String::from("user")),
            host: None,
        }, Prefix::parse(raw));

        assert!(test_prefix.is_ok());
        let test_prefix = test_prefix.unwrap();

        assert!(standard_prefix == test_prefix);
        assert!(test_prefix.to_string() == raw.to_string());
        assert!(test_prefix.len() == raw.len());
        assert!(!test_prefix.is_hostmask());
        assert!(test_prefix.is_server());
    }

    #[test]
    fn test_prefix_name_user_and_host() {
        let raw = "name!user@host";
        let (standard_prefix, test_prefix) = (Prefix {
            name: String::from("name"),
            user: Some(String::from("user")),
            host: Some(String::from("host")),
        }, Prefix::parse(raw));

        assert!(test_prefix.is_ok());
        let test_prefix = test_prefix.unwrap();

        assert!(standard_prefix == test_prefix);
        assert!(test_prefix.to_string() == raw.to_string());
        assert!(test_prefix.len() == raw.len());
        assert!(test_prefix.is_hostmask());
        assert!(!test_prefix.is_server());
    }

    #[test]
    fn test_prefix_name_and_host() {
        let raw = "name@host";
        let (standard_prefix, test_prefix) = (Prefix {
            name: String::from("name"),
            user: None,
            host: Some(String::from("host")),
        }, Prefix::parse(raw));

        assert!(test_prefix.is_ok());
        let test_prefix = test_prefix.unwrap();

        assert!(standard_prefix == test_prefix);
        assert!(test_prefix.to_string() == raw.to_string());
        assert!(test_prefix.len() == raw.len());
        assert!(!test_prefix.is_hostmask());
        assert!(test_prefix.is_server());
    }

    #[test]
    fn test_prefix_incorrect() {
        let test_prefix = Prefix::parse("!user@host");
        assert!(test_prefix.is_err());

        let test_prefix = Prefix::parse("name!@host");
        assert!(test_prefix.is_err());

        let test_prefix = Prefix::parse("name!@");
        assert!(test_prefix.is_err());

        let test_prefix = Prefix::parse("name!user@");
        assert!(test_prefix.is_err());

        let test_prefix = Prefix::parse("!@");
        assert!(test_prefix.is_err());

        let test_prefix = Prefix::parse("");
        assert!(test_prefix.is_err());
    }

    #[test]
    fn test_message_with_only_command() {
        let raw = "QUIT";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_prefix() {
        let raw = ":a!b@c QUIT";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_one_param() {
        let raw = "NICK john";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = "PASS oauth:token_goes_here";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_prefix_and_params() {
        let raw = ":WiZ!jto@tolsun.oulu.fi JOIN #Twilight_zone";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = ":WiZ!jto@tolsun.oulu.fi MODE #eu-opers -l";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_prefix_and_trailing() {
        let raw = ":syrk!kalt@millennium.stealth.net QUIT :Gone to have lunch";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = ":a@c NOTICE ::::Hey!";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_trailing() {
        let raw = "NICK :jon snow";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_params() {
        let raw = "USER jon snow example.com john";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = "MODE &oulu +b *!*@*.edu +e *!*@*.bu.edu";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_params_and_trailing() {
        let raw = "SERVER test.oulu.fi 1 :[tolsun.oulu.fi] Experimental server";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = "PRIVMSG #channel :Message with :colons!";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = "TOPIC #foo :";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_command_as_number() {
        let raw = ":irc.vives.lan 251 test :There are 2 users and 0 services on 1 servers";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = ":irc.vives.lan 250 test :Highest connection count: 1 (1 connections received)";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_prefix_params_and_trailing() {
        let raw = ":Trillian SQUIT cm22.eng.umd.edu :Server out of control";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = ":sorcix!~sorcix@sorcix.users.quakenet.org PRIVMSG \
                   #viveslan :\001ACTION is testing CTCP messages!\001";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = ":nick PRIVMSG $@ :This message contains a\ttab!";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());

        let raw = ":name!user@example.org PRIVMSG #test :Message with spaces at the end!  ";
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == raw.to_string());
        assert!(test_message.len() == raw.len());
    }

    #[test]
    fn test_message_with_extra_spaces() {
        let prefix = "WiZ";
        let command = "KICK";
        let params = "#Finnish John";
        let trailing = "Speaking English";

        let raw = &format!(":{}  {}   {} :{}", prefix, command, params, trailing);
        let normalized_raw = &format!(":{} {} {} :{}", prefix, command, params, trailing);
        let test_message = Message::parse(raw);

        assert!(test_message.is_ok());

        let test_message = test_message.unwrap();

        assert!(test_message.to_string() == normalized_raw.to_string());
        assert!(test_message.len() == normalized_raw.len());
    }

    #[test]
    fn test_message_invalid() {
        let test_message = Message::parse(":WiZ");
        assert!(test_message.is_err());

        let test_message = Message::parse(":WiZ :trailing parameter");
        assert!(test_message.is_err());

        let test_message = Message::parse("\r\n");
        assert!(test_message.is_err());

        let test_message = Message::parse("\n");
        assert!(test_message.is_err());

        let test_message = Message::parse(": PRIVMSG test :Invalid message with empty prefix.");
        assert!(test_message.is_err());

        let test_message = Message::parse(":  PRIVMSG test :Invalid message with space prefix");
        assert!(test_message.is_err());
    }
}
