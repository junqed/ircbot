use std::error::Error;
use std::fmt;
use std::io::{Read, Write, self};
use std::result;
use std::str;

use super::message::{Message, MessageError};

const ENDLINE: &'static str = "\r\n";

#[derive(Debug)]
pub enum DecodeError {
    Io(io::Error),
    Message(MessageError),
    Str(str::Utf8Error),
    Eof,
}

impl From<io::Error> for DecodeError {
    fn from(err: io::Error) -> DecodeError {
        DecodeError::Io(err)
    }
}

impl From<MessageError> for DecodeError {
    fn from(err: MessageError) -> DecodeError {
        DecodeError::Message(err)
    }
}

impl From<str::Utf8Error> for DecodeError {
    fn from(err: str::Utf8Error) -> DecodeError {
        DecodeError::Str(err)
    }
}

impl fmt::Display for DecodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DecodeError::Io(ref err) => write!(f, "IO error: {}", err),
            DecodeError::Message(ref err) => write!(f, "Message error: {}", err),
            DecodeError::Str(ref err) => write!(f, "Str error: {}", err),
            DecodeError::Eof => write!(f, "eof is reached"),
        }
    }
}

impl Error for DecodeError {
    fn description(&self) -> &str {
        match *self {
            DecodeError::Io(ref err) => err.description(),
            DecodeError::Message(ref err) => err.description(),
            DecodeError::Str(ref err) => err.description(),
            DecodeError::Eof => "eof is reached",
        }
    }
}

pub type DecodeResult<T> = result::Result<T, DecodeError>;

pub struct Decoder;

impl Decoder {
    pub fn decode<R: Read>(&self, mut r: R) -> DecodeResult<Message> {
        let mut buffer = [0; 512];

        let read_count = try!(r.read(&mut buffer));

        if read_count == 0 {
            return Err(DecodeError::Eof);
        }

        let buffer_as_str = try!(str::from_utf8(&buffer));
        let message = try!(Message::parse(buffer_as_str));

        Ok(message)
    }
}

#[derive(Debug)]
pub enum EncodeError {
    Io(io::Error),
}

impl From<io::Error> for EncodeError {
    fn from(err: io::Error) -> EncodeError {
        EncodeError::Io(err)
    }
}

impl fmt::Display for EncodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            EncodeError::Io(ref err) => write!(f, "IO error: {}", err),
        }
    }
}

impl Error for EncodeError {
    fn description(&self) -> &str {
        match *self {
            EncodeError::Io(ref err) => err.description(),
        }
    }
}

pub type EncodeResult<T> = result::Result<T, EncodeError>;

pub struct Encoder;

impl Encoder {
    pub fn encode<W: Write>(&self, w: W, message: &Message) -> EncodeResult<()> {
        let _ = try!(self.write(w, message.to_string().as_bytes()));
        Ok(())
    }

    pub fn write<W: Write>(&self, mut w: W, data: &[u8]) -> EncodeResult<usize> {
        let count = try!(w.write(&data));
        let endline_count = try!(w.write(&ENDLINE.as_bytes()));
        Ok(count + endline_count)
    }
}

#[cfg(test)]
mod tests {
    use std::io::{Write, Read, self};
    use std::error::Error;
    use super::{Encoder, Decoder};
    use ::irc::message::Message;

    struct WrongWriter;

    impl Write for WrongWriter {
        fn write(&mut self, _: &[u8]) -> io::Result<usize> {
            return Err(io::Error::new(io::ErrorKind::Other, "oh no!"));
        }

        fn flush(&mut self) -> io::Result<()> {
            Ok(())
        }
    }

    #[test]
    fn test_decoder_ok() {
        let test_str = ":irc.vives.lan 251 test :There 2 users and 0 services on 1 servers";
        let mut stream = test_str.as_bytes();

        let decoder = Decoder;
        assert!(decoder.decode(&mut stream).is_ok());
    }

    #[test]
    fn test_decoder_incorrect_message() {
        let test_str = ":irc.vives.lan";
        let mut stream = test_str.as_bytes();

        let decoder = Decoder;
        if let Err(err) = decoder.decode(&mut stream) {
            assert_eq!(err.description(), "Message parse error");
        } else {
            unreachable!();
        }
    }

    #[test]
    fn test_decoder_wrong_unicode_symbols() {
        let mut stream: &[u8] = &[0, 159, 146, 150];

        let decoder = Decoder;
        if let Err(err) = decoder.decode(&mut stream) {
            assert_eq!(err.description(), "invalid utf-8: corrupt contents");
        } else {
            unreachable!();
        }
    }

    #[test]
    fn test_decoder_eof_reached() {
        let test_str = ":irc.vives.lan 251 test :There 2 users and 0 services on 1 servers";
        let mut stream = test_str.as_bytes();

        let decoder = Decoder;
        decoder.decode(&mut stream).unwrap(); // read all

        if let Err(err) = decoder.decode(&mut stream) {
            assert_eq!(err.description(), "eof is reached");
        } else {
            unreachable!();
        }
    }

    #[test]
    fn test_decoder_io_error() {
        struct WrongReader;

        impl Read for WrongReader {
            fn read(&mut self, _: &mut [u8]) -> io::Result<usize> {
                return Err(io::Error::new(io::ErrorKind::Other, "oh no!"));
            }
        }

        let mut stream = WrongReader;

        let decoder = Decoder;
        if let Err(err) = decoder.decode(&mut stream) {
            assert_eq!(err.description(), "oh no!");
        } else {
            unreachable!();
        }
    }

    #[test]
    fn test_encoder_write_success() {
        let mut stream = vec![];

        let test_str: &[u8] = &[123, 124, 125];
        let encoder = Encoder;
        assert_eq!(encoder.write(&mut stream, &test_str).unwrap(), 5);
    }

    #[test]
    fn test_encoder_write_io_error() {
        let mut stream = WrongWriter;

        let test_str: &[u8] = &[123, 124, 125];
        let encoder = Encoder;

        if let Err(err) = encoder.write(&mut stream, &test_str) {
            assert_eq!(err.description(), "oh no!");
        } else {
            unreachable!();
        }
    }

    #[test]
    fn test_encoder_success_encode() {
        let mut stream = vec![];

        let test_command = ":irc.vives.lan 251 test :There 2 users and 0 services on 1 servers";
        let message = Message::parse(&test_command).unwrap();
        let encoder = Encoder;
        assert_eq!(encoder.encode(&mut stream, &message).unwrap(), ());
    }

    #[test]
    fn test_encoder_error_encode() {
        let mut stream = WrongWriter;

        let test_command = ":irc.vives.lan 251 test :There 2 users and 0 services on 1 servers";
        let message = Message::parse(&test_command).unwrap();
        let encoder = Encoder;

        if let Err(err) = encoder.encode(&mut stream, &message) {
            assert_eq!(err.description(), "oh no!");
        } else {
            unreachable!();
        }
    }
}
